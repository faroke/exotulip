//Use Filter
//fais un console log des valeurs divisibles par 2
//resultat [ 2, 4, 6 ]

function exo1() {
    let tab = [
        2,
        3,
        4,
        5,
        6,
        7
    ]
/*
    La méthode filter sert à créer un nouveau tableau avec les éléments qui correspondent à la condition donnée.
    @param e : la valeur courante
    @return: la valeur courante si elle est divisible par 2
*/
    console.log(tab.filter(e => {return e%2 == 0}))
}

exo1()
