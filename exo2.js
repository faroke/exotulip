//Use Reducer ONLY
//fais un console log du total des valeurs divisibles par 2 des total_amount
//resultat 12

function exo2() {
    let tab = [
        {
            otherdata: 2,
            total_Amount: 2,
            otherData: 4
        },
        {
            otherdata: 2,
            total_Amount: 3,
            otherData: 4
        },
        {
            otherdata: 2,
            total_Amount: 4,
            otherData: 4
        },
        {
            otherdata: 2,
            total_Amount: 5,
            otherData: 4
        },
        {
            otherdata: 2,
            total_Amount: 6,
            otherData: 4
        },
        {
            otherdata: 2,
            total_Amount: 7,
            otherData: 4
        }
    ]

/*
    La méthode reduce sert à accumuler plusieurs valeurs de notre tableau.
    @param callback : fonction qui prend en paramètre les valeurs accumulées et la valeur courante.
        @param sum: la valeur qui servira d'accumulateur
        @param e: la valeur qui servira de valeur courante nous permettant de manipuler le tableau
        @return: le paramètre sum avec la valeur de e.total_Amount pour lesquels module 2 est égale à 0 ajoutée à la suite
    @param valeurInitiale : la valeur qui servira d'accumulateur lors du premier appel de la fonction callback
*/

    console.log(tab.reduce((sum, e) => {
        return (e.total_Amount % 2 == 0) ? sum + e.total_Amount : sum;
    }, 0));
}

exo2()
