//Use a function and for only
//fais un console log du tableau tabResult
//en fonction de l'index de tab ajoute le number de tab a sa place dans tabResult
//resultat : [ 1, 1, 10, 3 ]

function exo3() {
    let tab = [{
        index: 2,
        number: 3
    },{
        index: 2,
        number: 5
    },{
        index: 3,
        number: 3
    },{
        index: 1,
        number: 1
    },{
        index: 0,
        number: 1
    },{
        index: 0,
        number: 0
    },{
        index: 2,
        number: 2
    }]
    let tabResult = [0,0,0,0]
/*
    La fonction sumTab sert à calculer la somme index d'un tableau.
        @param tab : Le tableau qui contient les valeurs à sommer
        @return: Rien.
*/
    const sumTab = function (tab){
        for(let i = 0; i < tab.length; i++){
            tabResult[tab[i].index] += tab[i].number
        }
    }
    sumTab(tab)
    console.log(tabResult)
}

exo3()
